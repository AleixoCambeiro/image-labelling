/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageLabelling;

import java.awt.Shape;

/**
 *
 * @author aleixo
 */
public class VOObject {
    
    //Attributes
    private int id = -1;
    
    private String name = null;
    private String pose = "Unspecified";
    private boolean truncated = false;
    private boolean difficult = false;
    private Shape box = null;
    
    
    //Constructors
    public VOObject() {
    }
    
    public VOObject(String name) {
        this.name = name;
    }
    
    public VOObject(String name, Shape box) {
        this.name = name;
        this.box = box;
    }
    
    public VOObject(String name, boolean truncated, boolean difficult, Shape box) {
        this.name = name;
        this.truncated = truncated;
        this.difficult = difficult;
        this.box = box;
    }
    
    
    //Setters & Getters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPose() {
        return pose;
    }

    public void setPose(String pose) {
        this.pose = pose;
    }

    public boolean isTruncated() {
        return truncated;
    }

    public void setTruncated(boolean truncated) {
        this.truncated = truncated;
    }

    public boolean isDifficult() {
        return difficult;
    }

    public void setDifficult(boolean difficult) {
        this.difficult = difficult;
    }

    public Shape getBox() {
        return box;
    }

    public void setBox(Shape box) {
        this.box = box;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
}
