/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageLabelling;

/**
 *
 * @author aleixo
 */
public class VOImage {
    
    //Attributes
    private String filename = null;
    private int width = -1;
    private int height = -1;
    private int depth = 3;
    
    
    //Constructors
    public VOImage() {
    }
    
    public VOImage(String filename, int width, int height) {
        this.filename = filename;
        this.width = width;
        this.height = height;
        this.depth = depth;
    }
    
    
    //Setters & Getters
    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }
    
    
}
