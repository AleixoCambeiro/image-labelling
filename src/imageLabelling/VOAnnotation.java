/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imageLabelling;

import java.util.ArrayList;

/**
 *
 * @author aleixo
 */
public class VOAnnotation {
    
    //Attributes
    private VOImage image = null;
    private ArrayList<VOObject> objects = null;
    
    
    //Constructors
    public VOAnnotation() {
    }
    
    public VOAnnotation(VOImage image, ArrayList<VOObject> objects) {
        this.image = image;
        this.objects = objects;
    }
    
    
    //Setters & Getters
    public VOImage getImage() {
        return image;
    }

    public void setImage(VOImage image) {
        this.image = image;
    }

    public ArrayList<VOObject> getObjects() {
        return objects;
    }

    public void setObjects(ArrayList<VOObject> objects) {
        this.objects = objects;
    }
    
    
}
