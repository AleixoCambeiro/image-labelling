/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package annotation.voc;

import java.awt.geom.Rectangle2D;

/**
 *
 * @author aleixo
 */
public class VOCBndbox extends VOCElement {

    //Attributes
    private Rectangle2D.Float box = null;
    
    //Constructor
    public VOCBndbox(int tabs) {
        super(tabs);
    }
    
    public VOCBndbox(int tabs, Rectangle2D.Float box) {
        super(tabs);
        this.box = box;
    }

    
    //Methods
    @Override
    public String toXML() {
        String xml = "";
        
        //VOC counts from 1 to image size, not from 0
        xml += tab(tabs) + "<bndbox>" + "\n";
        xml += tab(tabs) + "\t<xmin>" + ((int)this.box.getMinX() + 1) + "</xmin>" + "\n";
        xml += tab(tabs) + "\t<ymin>" + ((int)this.box.getMinY() + 1) + "</ymin>" + "\n";
        xml += tab(tabs) + "\t<xmax>" + ((int)this.box.getMaxX() + 1) + "</xmax>" + "\n";
        xml += tab(tabs) + "\t<ymax>" + ((int)this.box.getMaxY() + 1) + "</ymax>" + "\n";
        xml += tab(tabs) + "</bndbox>";
        
        return xml;
    }
    
    
    //Getters & Setters
    public Rectangle2D.Float getBox() {
        return box;
    }

    public void setBox(Rectangle2D.Float box) {
        this.box = box;
    }

    public int getTabs() {
        return tabs;
    }

    public void setTabs(int tabs) {
        this.tabs = tabs;
    }
    
    
}
