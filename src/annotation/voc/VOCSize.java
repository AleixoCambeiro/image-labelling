/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package annotation.voc;

/**
 *
 * @author aleixo
 */
public class VOCSize extends VOCElement {

    //Attributes
    private String width = null;
    private String height = null;
    private String depth = "3";
    
    
    //Constructor
    public VOCSize(int tabs) {
        super(tabs);
    }
    
    public VOCSize(int tabs, int width, int height) {
        super(tabs);
        
        this.width = "" + width;
        this.height = "" + height;
    }
    
    public VOCSize(int tabs, int width, int height, int depth) {
        super(tabs);
        
        this.width = "" + width;
        this.height = "" + height;
        this.depth = "" + depth;
    }

    
    //Methods
    @Override
    public String toXML() {
        String xml = "";
        
        xml += tab(tabs) + "<size>" + "\n";
        xml += tab(tabs) + "\t<width>" + this.width + "</width>" + "\n";
        xml += tab(tabs) + "\t<height>" + this.height + "</height>" + "\n";
        xml += tab(tabs) + "\t<depth>" + this.depth + "</depth>" + "\n";
        xml += tab(tabs) + "</size>";
        
        return xml;
    }
    
    
    //Setters & Getters
    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getDepth() {
        return depth;
    }

    public void setDepth(String depth) {
        this.depth = depth;
    }

    public int getTabs() {
        return tabs;
    }

    public void setTabs(int tabs) {
        this.tabs = tabs;
    }
    
    
}
