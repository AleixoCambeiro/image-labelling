/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package annotation.voc;

/**
 *
 * @author aleixo
 */
public class VOCOwner extends VOCElement {
    
    //Attributes
    private String flickrid = " ";
    private String name = " ";

    
    //Constructor
    public VOCOwner(int tabs) {
        super(tabs);
    }

    
    //Methods
    @Override
    public String toXML() {
        String xml = "";
        
        xml += tab(tabs) + "<owner>" + "\n";
        xml += tab(tabs) + "\t<flickrid>" + this.flickrid + "</flickrid>" + "\n";
        xml += tab(tabs) + "\t<name>" + this.name + "</name>" + "\n";
        xml += tab(tabs) + "</owner>";
        
        return xml;
    }
    
    
    //Getters & Setters
    public String getFlickrid() {
        return flickrid;
    }

    public void setFlickrid(String flickrid) {
        this.flickrid = flickrid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTabs() {
        return tabs;
    }

    public void setTabs(int tabs) {
        this.tabs = tabs;
    }
    
}
