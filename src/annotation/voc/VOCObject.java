/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package annotation.voc;

import imageLabelling.VOObject;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author aleixo
 */
public class VOCObject extends VOCElement {

    //Attributes
    private String name = "boat";
    private String pose = "Unspecified";
    private String truncated = "0";
    private String difficult = "0";
    private VOCBndbox box = null;
    
    //Constructor
    public VOCObject(int tabs) {
        super(tabs);
    }
    
    public VOCObject(int tabs, Rectangle2D.Float box) {
        super(tabs);
        this.box = new VOCBndbox(tabs + 1, box);
    }
    
    public VOCObject(int tabs, VOObject o) {
        super(tabs);
        this.name = o.getName();
        this.pose = o.getPose();
        this.truncated = boolToString(o.isTruncated());
        this.difficult = boolToString(o.isDifficult());
        
        Rectangle2D.Float box = (Rectangle2D.Float)o.getBox();
        this.box = new VOCBndbox(tabs + 1, box);
    }

    
    //Methods
    @Override
    public String toXML() {
        String xml = "";
        
        xml += tab(tabs) + "<object>" + "\n";
        xml += tab(tabs) + "\t<name>" + this.name + "</name>" + "\n";
        xml += tab(tabs) + "\t<pose>" + this.pose + "</pose>" + "\n";
        xml += tab(tabs) + "\t<truncated>" + this.truncated + "</truncated>" + "\n";
        xml += tab(tabs) + "\t<difficult>" + this.difficult + "</difficult>" + "\n";
        xml += this.box.toXML() + "\n";
        xml += tab(tabs) + "</object>";
        
        return xml;
    }
    
    private String boolToString(boolean b){
        if(b) return "1";
        return "0";
    }
    
    
    //Setters & Getters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPose() {
        return pose;
    }

    public void setPose(String pose) {
        this.pose = pose;
    }

    public String getTruncated() {
        return truncated;
    }

    public void setTruncated(String truncated) {
        this.truncated = truncated;
    }

    public String getDifficult() {
        return difficult;
    }

    public void setDifficult(String difficult) {
        this.difficult = difficult;
    }

    public VOCBndbox getBox() {
        return box;
    }

    public void setBox(VOCBndbox box) {
        this.box = box;
    }

    public int getTabs() {
        return tabs;
    }

    public void setTabs(int tabs) {
        this.tabs = tabs;
    }
    
}
