/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package annotation.voc;

import imageLabelling.VOAnnotation;
import imageLabelling.VOImage;
import imageLabelling.VOObject;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

/**
 *
 * @author aleixo
 */
public class VOCAnnotation extends VOCElement {
    
    //Attributes
    private String folder = "VOC2007";
    private String filename = null;
    private VOCElement source = new VOCSource(tabs + 1);
    private VOCElement owner = new VOCOwner(tabs + 1);
    private VOCElement size = null;
    private String segmented = "0";
    private ArrayList<VOCElement> objects = new ArrayList<>();
    
    //Constructor
    public VOCAnnotation(int tabs) {
        super(tabs);
    }
    
    public VOCAnnotation(int tabs, String filename, int width, int height, ArrayList<Shape> boxes) {
        super(tabs);
        
        this.filename = filename;
        this.size = new VOCSize(tabs + 1, width, height);
        
        for(Shape s : boxes){
            Rectangle2D.Float r = (Rectangle2D.Float) s.getBounds2D();
            this.objects.add(new VOCObject(tabs + 1, r));
        }
    }
    
    public VOCAnnotation(VOAnnotation anno, String folder) {
        super(0); //No tabs
        
        VOImage image = anno.getImage();
        
        this.filename = image.getFilename();
        this.size = new VOCSize(this.tabs + 1, image.getWidth(),
                                image.getHeight(), image.getDepth());
        
        if((folder != null) && (!folder.trim().isEmpty())) this.folder = folder;
        
        for(VOObject o : anno.getObjects())
            this.objects.add(new VOCObject(tabs + 1, o));
    }

    
    //Methods
    @Override
    public String toXML() {
        String xml = "";
        
        xml += tab(tabs) + "<annotation>" + "\n";
        xml += tab(tabs) + "\t<folder>" + this.folder + "</folder>" + "\n";
        xml += tab(tabs) + "\t<filename>" + this.filename + "</filename>" + "\n";
        xml += this.source.toXML() + "\n";
        xml += this.owner.toXML() + "\n";
        xml += this.size.toXML() + "\n";
        xml += tab(tabs) + "\t<segmented>" + this.segmented + "</segmented>" + "\n";
        for(VOCElement o : this.objects)
            xml += o.toXML() + "\n";
        xml += tab(tabs) + "</annotation>";
        
        return xml;
    }
    
    
    //Setters & Getters

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public VOCElement getSource() {
        return source;
    }

    public void setSource(VOCElement source) {
        this.source = source;
    }

    public VOCElement getOwner() {
        return owner;
    }

    public void setOwner(VOCElement owner) {
        this.owner = owner;
    }

    public String getSegmented() {
        return segmented;
    }

    public void setSegmented(String segmented) {
        this.segmented = segmented;
    }

    public ArrayList<VOCElement> getObjects() {
        return objects;
    }

    public void setObjects(ArrayList<VOCElement> objects) {
        this.objects = objects;
    }

    public int getTabs() {
        return tabs;
    }

    public void setTabs(int tabs) {
        this.tabs = tabs;
    }
    
    
}
