/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package annotation.voc;

/**
 *
 * @author aleixo
 */
public class VOCSource extends VOCElement{
    
    //Attributes
    private String database = " ";
    private String annotation = " ";
    private String image = " ";
    private String flickrid = " ";
    
    
    //Constructor
    public VOCSource(int tabs) {
        super(tabs);
    }
    
    
    //Methods
    @Override
    public String toXML(){
        String xml = "";
        
        xml += tab(tabs) + "<source>" + "\n";
        xml += tab(tabs) + "\t<database>" + this.database + "</database>" + "\n";
        xml += tab(tabs) + "\t<annotation>" + this.annotation + "</annotation>" + "\n";
        xml += tab(tabs) + "\t<image>" + this.image + "</image>" + "\n";
        xml += tab(tabs) + "\t<flickrid>" + this.flickrid + "</flickrid>" + "\n";
        xml += tab(tabs) + "</source>";
        
        return xml;
    }
    
    
    //Setters & Getters

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFlickrid() {
        return flickrid;
    }

    public void setFlickrid(String flickrid) {
        this.flickrid = flickrid;
    }

    public int getTabs() {
        return tabs;
    }

    public void setTabs(int tabs) {
        this.tabs = tabs;
    }
    
}
