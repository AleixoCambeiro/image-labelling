/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package annotation.voc;

/**
 *
 * @author aleixo
 */
public abstract class VOCElement {
    
    //Attributes
    protected int tabs = 0;
    
    
    //Constructor
    public VOCElement(int tabs) {
        this.tabs = tabs;
    }
    
    
    
    //Methods
    protected String tab(int n){
        String tab = "";
        
        for(int i=0; i<n; i++) tab += "\t";
        
        return tab;
    }
    
    public abstract String toXML();
}
